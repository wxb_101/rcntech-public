#!/bin/bash

# JVM_OPS: 虚拟机参数 -Xmx512m -Xms512
# APP_OPS: App启动参数 -Dspring.profiles.active=dev
# APP_JAR_FILE: 打包后的Jar文件
# 检查第一个参数
 
workdir=""
jvmOps=""
appArgs=""
imageTag=""
jarFile=""
useDefaultLogfile=true
repository="https://gitlab.com/yjwfn/rcntech-public"

usage(){
    echo "usage: "
    echo "-d: 指定工作目录"
    echo "-t: 镜像tag"
    echo "-j: Java虚拟机参数"
    echo "-a: App启动参数"
    echo "-f: jar文件名称"
    echo "-e: 不使用默认日志配置"
}


if [[ -z $* ]]
then
    usage
    exit 1
fi


echo "args: $@"

while getopts ":hed:t:j:a:f:"  args
do
    case $args in
        t)
            imageTag="$OPTARG"
            ;;
        d)
            workdir="$OPTARG"
            ;;
        j)
            jvmOps="$OPTARG"
            ;;
        a)
             appArgs="$OPTARG"
            ;;
        f) jarFile="$OPTARG";;
        e) useDefaultLogfile=false;;
        h) usage ;;
        ?) usage ;;
    esac
done

jvmOps="$jvmOps -Dcom.alibaba.nacos.naming.log.level=error -Dcom.alibaba.nacos.config.log.level=error"


if [[ ! -e ${workdir} ]]
then
    echo "error: Not found workdir: ${workdir}"
    exit 1
fi

cd ${workdir}

# 下载日志文件
if ${useDefaultLogfile}
then
    logfile=logback-spring.xml
    rm -f ${logfile}
    if [[ ${CI_BUILD_REF_NAME} = "master" ]]
    then
        wget -O ${logfile} ${repository}/raw/master/logback-prod.xml
    else    
        wget -O ${logfile} ${repository}/raw/master/logback-spring.xml
    fi     

    chmod 755 ${logfile}
fi


package (){
    # 打包成Jar
    which "mvn" > /dev/null
    if [[ $? -ne 0 ]]
    then
       echo "Install maven"
       apt-get update
       apt-get install mvn
    fi

    echo "Build jar"
    mvn package


    # 编译镜像
    which "docker" > /dev/null
    if [[ $? -ne 0 ]]
    then
        echo "error: Not found docker command"
        exit 1
    fi
}


createDockerfile(){
    # 生成entrypoint.sh
    # 如果没有entrypoint.sh则生成默认的
    Dockerfile="Dockerfile"
    ENTRYPOINT="entrypoint.sh"

    #删除之前的ENTRYPOINT
    if [[ -e  ${ENTRYPOINT} ]]
    then
        rm -f ${ENTRYPOINT} >> /dev/null
    fi

    touch ${ENTRYPOINT}
    chmod 755 ${ENTRYPOINT}


    logopt=""
    if [[ ! -z ${logfile} ]]
    then
        logopt="-Dlogging.config=/app/${logfile} "
    fi


    echo "java -jar ${jvmOps} ${logopt} ${appArgs} /app/${jarFile} " >> ${ENTRYPOINT}

    #删除Dockerfile
    if [[  -e ${Dockerfile} ]]
    then
        rm -f ${Dockerfile} >> /dev/null
    fi

    # 生成Dockerfile
    touch ${Dockerfile}
    echo 'FROM registry-vpc.cn-hangzhou.aliyuncs.com/ydtech-egg/egg-server:1.0.0' >> ${Dockerfile}
    echo 'MAINTAINER yjwfn' >> ${Dockerfile}
    echo 'ARG APP_JAR_FILE' >> ${Dockerfile}
    

     
    if [[ -e ${logfile}  ]]
    then
         echo "COPY ${logfile} /app/${logfile}" >> ${Dockerfile}
    fi

    echo 'COPY ${APP_JAR_FILE} /app/${jarFile}' >> ${Dockerfile}
    echo "COPY ${ENTRYPOINT} /app/${ENTRYPOINT}" >> ${Dockerfile}
    echo 'WORKDIR /app' >> ${Dockerfile}
    echo "ENTRYPOINT [\"sh\", \"/app/${ENTRYPOINT}\"]" >> ${Dockerfile}
}


buildImage(){
    if [[ -z ${imageTag} ]]
    then
        echo "error: 没有镜像Tag参数"
        usage
        exit 1
    fi

    if [[ -z ${jarFile} ]]
    then
        echo "error: Jar文件名称不能为空"
        usage
        exit 1
    fi


    createDockerfile
    echo "Delete image from last build"
    docker image rm ${imageTag} > /dev/null || true
    echo "Try to build image with name ${imageTag}"
    docker build --tag ${imageTag} --network=host --build-arg APP_JAR_FILE=target/${jarFile}  .
}



package
buildImage
